from django import forms

occupation_list = ['Select an occupation', 'Activist', 'Actor', 'Actress', 'Animator', 'Analyst', 'Athlete',
'Author', 'Band', 'Baseball Player', 'Basketball Player', 'Boxer', 'Businessman', 'Businesswoman', 'Chef', 
'Comedian', 'Composer', 'Cricket Player', 'Cyclist', 'DJ', 'Dancer', 'Designer', 'Director', 'Economist',
'Figure Skaters', 'Film Critic', 'Film Producer', 'Filmmaker', 'Football Analyst', 'Football Player', 
'Golfer', 'Hockey Player', 'Internet Personality', 'Journalist', 'Lobbyist', 'Magician', 
'Mixed Martial Artist', 'Model', 'Motorcycle Racer', 'Musician', 'Pastor', 'Political Analyst', 
'Politician', 'Producer', 'Race Car Driver', 'Radio Host', 'Rapper', 'Reality TV', 'Singer', 'Soccer Player',
'Speaker', 'TV Commercial Cast', 'TV Executives', 'TV Host', 'TV Personality', 'TV Producer', 'TV Show Cast',
'Tennis Player'	, 'Theater', 'Theater Duo', 'Track Athlete', 'Wrestler', 'Writer']

occupation_option = []

for i in range(len(occupation_list)):
    if i == 0:
        occupation_option.append(('', occupation_list[i]))
    else:
        occupation_option.append((occupation_list[i], occupation_list[i]))

class QueryForms(forms.Form):
    name = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                'class':'form-control', 'id':'name', 'placeholder':'Enter celebrity/event/product name',
            }
        ),
    required = False)

    occupation = forms.CharField(
        widget = forms.Select(
            choices = occupation_option,
            attrs = {
                'class':'form-control', 'id':'occupation',
            }
        ),
    required = False)

    year = forms.IntegerField(
        widget = forms.NumberInput(
            attrs = {
                'class':'form-control', 'id':'name', 'placeholder':'Enter forbes year', 'min':'1999', 'max':'2020',
            }
        ),
    required = False)

    rank = forms.IntegerField(
        widget = forms.NumberInput(
            attrs = {
                'class':'form-control', 'id':'name', 'placeholder':'Enter forbes rank', 'min':'1', 'max':'100',
            }
        ),
    required = False)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)