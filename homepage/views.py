from django.shortcuts import render
from .forms import QueryForms
from .rdf_query import results_from_local, results_from_dbpedia, results_local_from_name

# Create your views here.
def home(request):
    context = {
        'query_forms' : QueryForms
    }
    return render(request, 'homepage.html', context)

def home_results(request):
    context = {
        'query_forms' : QueryForms
    }
    if request.method == 'POST':
        received_form = QueryForms(request.POST)
        if (request.POST['name'] or request.POST['year'] or request.POST['rank'] or request.POST['occupation']) == "":
            res = ""
        else:
            res = results_from_local(request.POST['name'], request.POST['year'], request.POST['rank'], request.POST['occupation'])
        context['query_forms'] = received_form
        context['query_res'] = res
        return render(request, 'results.html', context)

def celeb_detail(request, value):
    value_rep = value.replace(" ","_")
    response = {"name": value_rep}
    dbpedia_query_res = results_from_dbpedia(response["name"])
    local = results_local_from_name(value)
    dbpedia = dbpedia_query_res[0]

    if(len(dbpedia) != 0):       
        if ('description' in dbpedia):
            response['description'] = dbpedia['description']['value']
        else:
            response['description'] = " - "

        if ('startDate' in dbpedia):
            response['startDate'] = dbpedia['startDate']['value']
        else:
            response['startDate'] = " - "
       
        if ('birthDate' in dbpedia):
            response['birthDate'] = dbpedia['birthDate']['value']
        else:
            response['birthDate'] = " - "
        
        if ('birthPlace' in dbpedia):
            response['birthPlace_link'] = dbpedia['birthPlace']['value']
            response['birthPlace'] = response['birthPlace_link'].split("/")[-1].replace('_', " ")
        else:
            response['birthPlace_link'] = " - "
            response['birthPlace'] = " - "
        
        if ('celebName' in dbpedia):
            response['celebName'] = dbpedia['celebName']['value'].replace('_'," ")

        if ('thumbnail' in dbpedia):
            response['thumbnail'] = dbpedia['thumbnail']['value']  
        
        response['occupation'] = local[0]['occupation']

        response['yearRank'] = []
        for data in local:
            response['yearRank'].append(data['yearRank'])

    return render(request, 'infobox_celeb.html', response)


