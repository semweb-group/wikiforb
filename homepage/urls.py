from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('results/', views.home_results, name='home_results'),
    path('celeb_detail/<str:value>', views.celeb_detail, name='celeb_detail'),
]