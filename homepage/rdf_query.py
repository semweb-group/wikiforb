from SPARQLWrapper import SPARQLWrapper
from rdflib import Graph

def results_from_local(celeb_name, input_year, input_rank, occupation):
    graph = Graph()
    result = graph.parse('forbes_data.ttl', format='n3')

    years = []
    start = 1999
    for i in range(22):
        year = start + i
        years.append(year)

    final_result = []
    for year in years:
        if len(input_rank) != 0 and len(input_year) != 0:
            query = """PREFIX ex: <http://example.org/>
                SELECT ?celebName ?year ?rank ?occupation
                WHERE {
                    ?celeb ex:name ?celebName
                    FILTER (regex(?celebName, "%s", "i")) .
                    ?celeb ex:forbes%sYear ?year .
                    FILTER (xsd:integer(?year) = %d) .
                    ?celeb ex:forbes%sRank ?rank .
                    FILTER (xsd:integer(?rank) = %d) .
                    ?celeb ex:occupation ?occupation .
                    FILTER (regex(?occupation, "%s", "i")) .
                } """ % (celeb_name, year, int(input_year), year, int(input_rank), occupation)
        elif len(input_rank) == 0 and len(input_year) == 0:
            query = """PREFIX ex: <http://example.org/>
                SELECT ?celebName ?year ?rank ?occupation
                WHERE {
                    ?celeb ex:name ?celebName
                    FILTER (regex(?celebName, "%s", "i")) .
                    ?celeb ex:forbes%sYear ?year .
                    ?celeb ex:forbes%sRank ?rank .
                    ?celeb ex:occupation ?occupation .
                    FILTER (regex(?occupation, "%s", "i")) .
                } """ % (celeb_name, year, year, occupation)
        elif len(input_rank) == 0:
            query = """PREFIX ex: <http://example.org/>
                SELECT ?celebName ?year ?rank ?occupation
                WHERE {
                    ?celeb ex:name ?celebName
                    FILTER (regex(?celebName, "%s", "i")) .
                    ?celeb ex:forbes%sYear ?year .
                    FILTER (xsd:integer(?year) = %d) .
                    ?celeb ex:forbes%sRank ?rank .
                    ?celeb ex:occupation ?occupation .
                    FILTER (regex(?occupation, "%s", "i")) .
                } """ % (celeb_name, year, int(input_year), year, occupation)
        elif len(input_year) == 0:
            query = """PREFIX ex: <http://example.org/>
                SELECT ?celebName ?year ?rank ?occupation
                WHERE {
                    ?celeb ex:name ?celebName
                    FILTER (regex(?celebName, "%s", "i")) .
                    ?celeb ex:forbes%sYear ?year .
                    ?celeb ex:forbes%sRank ?rank .
                    FILTER (xsd:integer(?rank) = %d) .
                    ?celeb ex:occupation ?occupation .
                    FILTER (regex(?occupation, "%s", "i")) .
                } """ % (celeb_name, year, year, int(input_rank), occupation)
                
        query_result = graph.query(query)
        for result in query_result:
            result = result.asdict()
            row = []
            for key in result:
                row.append(result[key].toPython())
            row = tuple(row)
            final_result.append(row)  
    
    final_result = list(set(final_result))
    final_final_result = []
    for item in final_result:
        value = list(item)
        key = ["celebName", "year", "rank", "occupation", "url"]
        value.append(("&".join(item)).replace(" ", "%20"))
        final_final_result.append(dict(zip(key, value)))
    return final_final_result

def results_local_from_name(celeb_name):
    graph = Graph()
    result = graph.parse('forbes_data.ttl', format='n3')

    years = []
    start = 1999
    for i in range(22):
        year = start + i
        years.append(year)

    final_result = []
    for year in years:
        query = """PREFIX ex: <http://example.org/>
            SELECT ?celebName ?occupation (GROUP_CONCAT(CONCAT(?year, "-", ?rank) ; SEPARATOR = ", ") AS ?yearRank)
            WHERE {
                ?celeb ex:name ?celebName
                FILTER (regex(?celebName, "%s", "i")) .
                ?celeb ex:forbes%sYear ?year .
                ?celeb ex:forbes%sRank ?rank .
                ?celeb ex:occupation ?occupation .
            } GROUP BY ?celebName ?occupation """ % (celeb_name, year, year)

        query_result = graph.query(query)
        for result in query_result:
            result = result.asdict()
            row = []
            for key in result:
                row.append(result[key].toPython())
            row = tuple(row)
            final_result.append(row)  
    
    final_result = list(set(final_result))
    final_final_result = []
    for item in final_result:
        value = list(item)
        key = ["celebName", "occupation", "yearRank"]
        value.append(("&".join(item)).replace(" ", "%20"))
        final_final_result.append(dict(zip(key, value)))
    return final_final_result

def results_from_dbpedia(celeb_name):
    sparql = SPARQLWrapper("http://dbpedia.org/sparql")
    query = """
        PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
        PREFIX dbp: <http://dbpedia.org/property/>
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX ex: <http://example.org/>
        SELECT ?description ?celebName ?startDate ?birthDate ?birthPlace ?thumbnail
        WHERE { 
            dbr:%s rdfs:label ?celebName .
            FILTER (lang(?celebName) = 'en')            
            OPTIONAL { 
            dbr:%s dbo:abstract ?description . 
            FILTER (lang(?description) = 'en')
            }
            OPTIONAL {
                dbr:%s dbo:birthDate | dbp:birthDate ?birthDate . 
                FILTER (datatype(?birthDate) = xsd:date)
            }
            OPTIONAL { dbr:%s dbo:birthPlace | dbp:birthPlace ?birthPlace . }
            OPTIONAL { dbr:%s dbo:activeYearsStartYear ?startDate . }
            OPTIONAL { dbr:%s dbo:thumbnail ?thumbnail . }                
        }
        LIMIT 1""" % (celeb_name, celeb_name,celeb_name,celeb_name,celeb_name, celeb_name)
    
    sparql.setQuery(query)
    sparql.setReturnFormat("json")
    results = sparql.query().convert()
    return results["results"]["bindings"]

if __name__ == '__main__':
    results_from_dbpedia("BTS")